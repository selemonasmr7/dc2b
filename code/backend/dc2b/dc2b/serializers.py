from rest_framework import serializers
from .models import DecimalBinaryPair


class DecimalBinaryPairSerializer(serializers.ModelSerializer):
    class Meta:
        model = DecimalBinaryPair
        fields = '__all__'

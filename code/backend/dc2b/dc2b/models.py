from django.db import models


class DecimalBinaryPair(models.Model):
    decimal_value = models.DecimalField(max_digits=10, decimal_places=2)
    binary_value = models.CharField(max_length=64)

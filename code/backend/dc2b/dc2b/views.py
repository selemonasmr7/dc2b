# yourapp/views.py
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views import View
from .models import DecimalBinaryPair


class DecimalToBinaryView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        try:
            decimal_value = float(request.POST.get('decimal_value'))
            binary_value = bin(int(decimal_value))[2:]  # Convert to binary

            pair = DecimalBinaryPair.objects.create(
                decimal_value=decimal_value,
                binary_value=binary_value
            )

            return JsonResponse({
                'decimal_value': pair.decimal_value,
                'binary_value': pair.binary_value
            })

        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)
